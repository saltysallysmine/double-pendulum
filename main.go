package main

import (
	"flag"
	"fmt"
	"image/color"
	"log"
	"math"
	"sync"

	. "double_pendulum/drawer"
	. "double_pendulum/engineer"
	"double_pendulum/internal/config"

	"github.com/hajimehoshi/ebiten/v2"
)

func GetConfig() *config.Config {
	var cfgPath string
	flag.StringVar(&cfgPath, "cfg", "examples/config.yaml",
		"path to application config")
	flag.Parse()

	cfg, err := config.NewConfig(cfgPath)
	if err != nil {
		log.Fatal(fmt.Errorf("Can not make config. %s", err))
	}

	return cfg
}

// Configuration

var windowSize = Point{X: 1600, Y: 1600}
var screenSize = Point{X: 1600, Y: 1600}
var translation = Point{X: windowSize.X / 2, Y: windowSize.Y / 2}

var pendsCnt int
var traces []Trace
var pends []DoublePendulum
var colors []color.Color
var trace = false
var tail = false

var dr = NewDrawer(translation)

func Initialize(cfg *config.Config) {
	pendsCnt = cfg.PendsCnt

	pends = make([]DoublePendulum, pendsCnt)
	traces = make([]Trace, pendsCnt)
	colors = make([]color.Color, pendsCnt)

	difference := cfg.Difference

	InitializeEngineer(cfg)

	for i := 0; i < pendsCnt; i++ {
		pends[i] = NewCustomisedDP(
			cfg.R1,
			cfg.R2,
			cfg.M1,
			cfg.M2,
			math.Pi/2+float64(i)*difference,
			math.Pi/2+float64(i)*difference,
		)
		traces[i] = NewTrace()
		colors[i] = GetGradient(uint32(i), uint32(pendsCnt))
	}
}

// Simulation

type Game struct{}

func (g *Game) Update() error {
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	var wg sync.WaitGroup
	wg.Add(pendsCnt)

	movePend := func(screen *ebiten.Image, pend *DoublePendulum, pendColor color.Color, ind int) {
		defer wg.Done()
		pend.Move()
	}

	for i := 0; i < pendsCnt; i++ {
		go movePend(screen, &pends[i], colors[i], i)

		if trace {
			_, p2 := pends[i].GetCoordinates()
			traces[i].AddPoint(p2)
			dr.DrawTrace(screen, &traces[i], colors[i])
		}

		if tail {
			dr.DrawPendulumTail(screen, &pends[i], colors[i])
		} else {
			dr.DrawPendulum(screen, &pends[i], colors[i])
		}
	}

	wg.Wait()
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return int(screenSize.X), int(screenSize.Y)
}

func main() {
	cfg := GetConfig()
	trace = cfg.Trace
	tail = cfg.Tail

	Initialize(cfg)

	ebiten.SetFullscreen(true)
	ebiten.SetWindowSize(int(windowSize.X), int(windowSize.Y))
	ebiten.SetWindowTitle("Double pendulum")
	if err := ebiten.RunGame(&Game{}); err != nil {
		log.Fatal(err)
	}
}
