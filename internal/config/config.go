package config

import (
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Grav  float64 `yaml:"grav"`
	Speed float64 `yaml:"speed"`

	PendsCnt   int     `yaml:"pendsCnt"`
	Difference float64 `yaml:"difference"`

	R1 float64 `yaml:"r1"`
	R2 float64 `yaml:"r2"`
	M1 float64 `yaml:"m1"`
	M2 float64 `yaml:"m2"`

	Trace bool `yaml:"trace"`
	Tail  bool `yaml:"tail"`
}

func NewConfig(filePath string) (*Config, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		return nil, err
	}

	return &cfg, nil
}
