package engineer

import (
	"math"
)

type DoublePendulum struct {
	r1   float64
	r2   float64
	m1   float64
	m2   float64
	a1   float64
	a2   float64
	a1_v float64
	a2_v float64
	a1_a float64
	a2_a float64
}

func NewDP() DoublePendulum {
	dp := DoublePendulum{
		r1:   200,
		r2:   200,
		m1:   4,
		m2:   4,
		a1:   math.Pi / 2,
		a2:   math.Pi / 2,
		a1_v: 0,
		a2_v: 0,
	}
	return dp
}

func NewCustomisedDP(r1, r2, m1, m2, a1, a2 float64) DoublePendulum {
	dp := DoublePendulum{
		r1:   r1,
		r2:   r2,
		m1:   m1,
		m2:   m2,
		a1:   a1,
		a2:   a2,
		a1_v: 0,
		a2_v: 0,
	}
	return dp
}

func (dp *DoublePendulum) GetCoordinates() (Point, Point) {
	x1 := dp.r1 * math.Sin(dp.a1)
	y1 := dp.r1 * math.Cos(dp.a1)
	p1 := Point{
		X: x1,
		Y: y1,
	}

	x2 := x1 + dp.r2*math.Sin(dp.a2)
	y2 := y1 + dp.r2*math.Cos(dp.a2)
	p2 := Point{
		X: x2,
		Y: y2,
	}

	return p1, p2
}

func (dp *DoublePendulum) calcNewAcceleration() {
	num_term1 := (-G * (2*dp.m1 + dp.m2)) * math.Sin(dp.a1)
	num_term2 := -dp.m2 * G * math.Sin(dp.a1-2*dp.a2)
	num_term3 := -2 * dp.m2 * math.Sin(dp.a1-dp.a2)
	num_term4 := dp.a2_v*dp.a2_v*dp.r2 + dp.a1_v*dp.a1_v*dp.r1*math.Cos(dp.a1-dp.a2)
	denum := dp.r1 * (2*dp.m1 + dp.m2 - dp.m2*math.Cos(2*dp.a1-2*dp.a2))
	dp.a1_a = (num_term1 + num_term2 + num_term3*num_term4) / denum

	num_term1 = 2 * math.Sin(dp.a1-dp.a2)
	num_term2 = dp.a1_v * dp.a1_v * dp.r1 * (dp.m1 + dp.m2)
	num_term3 = G * (dp.m1 + dp.m2) * math.Cos(dp.a1)
	num_term4 = dp.a2_v * dp.a2_v * dp.r2 * dp.m2 * math.Cos(dp.a1-dp.a2)
	denum = dp.r2 * (2*dp.m1 + dp.m2 - dp.m2*math.Cos(2*dp.a1-2*dp.a2))
	dp.a2_a = (num_term1 * (num_term2 + num_term3 + num_term4)) / denum
}

func (dp *DoublePendulum) normalizeAnlge(angle *float64) {
	if math.Abs(*angle) >= 2*math.Pi {
		delta := (math.Abs(*angle) / (2 * math.Pi)) * 2 * math.Pi
		if *angle < 0. {
			*angle += delta
		} else {
			*angle -= delta
		}
	}
}

func (dp *DoublePendulum) normalizeAngles() {
	dp.normalizeAnlge(&dp.a1)
	dp.normalizeAnlge(&dp.a2)
}

func (dp *DoublePendulum) Move() {
	dp.calcNewAcceleration()

	dp.a1_a *= Speed
	dp.a2_a *= Speed
	dp.a1_v += dp.a1_a
	dp.a2_v += dp.a2_a
	dp.a1 += dp.a1_v
	dp.a2 += dp.a2_v

	dp.normalizeAngles()
}
