package engineer

import "double_pendulum/internal/config"

var (
	G     = 9.8066
	Speed = 0.05
)

func InitializeEngineer(cfg *config.Config) {
	G = cfg.Grav
	Speed = cfg.Speed
}
