package engineer

type Trace struct {
	Points []Point
	Ind    int
}

func NewTrace() Trace {
	return Trace{
		Points: make([]Point, 100),
		Ind:    0,
	}
}

func (tr *Trace) AddPoint(p Point) {
	tr.Points[tr.Ind%cap(tr.Points)] = p
	tr.Ind = (tr.Ind + 1) % cap(tr.Points)
}
