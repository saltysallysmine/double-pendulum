#let conf(title, doc) = {
  set page(
    header: align(
      right,
      title
    )
  )
  set text(size: 12pt)
  set par(justify: true)
  doc
}

#show: doc => conf(
  [Double-pendulum],
  doc,
)

#let delimiter = rect(
  width: 100%,
  height: 1pt,
  inset: 4pt,
  fill: black,
)

#set page(numbering: "— 1 —")

#set math.equation(numbering: "(1)")

#show link: underline

// Content

= Двойной математический маятник


*Двойной маятник* -- это математический маятник, к концу которого прикреплён другой математический маятник. Двойной маятник является простой физической системой, которая проявляет разнообразное поведение со значительной зависимостью от начальных условий. Движение маятника руководствуется связанными обыкновенными дифференциальными уравнениями.


#align(center)[
  #image("./img/2023-12-16_13-14.jpg", width: 70%)
]

== Физическая модель

$
cases(
  x_1^2 + y_1^2 = l_1^2,
  (x_2 - x_1)^2 + (y_2 - y_1)^2 = l_2^2
)
$

Система имеет две степени свободы $=>$ нужно две обобщённых координаты.

$
cases(
  x_1 = l_1 sin(phi),
  y_1 = l_1 cos(phi)
) => cases(
  T_1 = (m_1 l_1^2 dot(phi)^2) / 2 " - кинетическая энергия",
  U_1 = m g l_1 (1 - cos(phi)) " - потенциальная энергия."
)
$

Также для второй пары координат:

$
cases(
  x_2 = l_1 sin(phi) + l_2 sin(psi),
  y_2 = l_1 cos(phi) + l_2 cos(psi),
)
$

$
cases(
  T_2 = m_2 / 2 (dot(x_2) + dot(y_2)),
  U_2 = m_2 g h_2 = m_2 g (l_1 + l_2) - m_2 g y_2
) => cases(
  T_2 = m_2 / 2 (l_1^2 dot(phi)^2 + l_2^2 dot(psi)^2 + 2 l_1 l_2 cos(phi - psi) dot(phi) dot(psi)),
  U_2 = m g l_1 (1 - cos(phi))
)
$

*Лагранжиан* $L[phi_i]$ динамической системы -- функция обобщённых координат $phi_i(s)$, которая описывает развитие системы.

$
L = T - U
$

$
d/(d t) (diff(L) / diff(dot(q_i))) - diff(L) / diff(q_i) = 0.
$

После вычислений получаем:

$
cases(
  dot.double(phi) + Omega^2 sin(phi) + m_2 / M l_2 / l_1 (dot.double(psi) cos(phi - psi) - dot(psi)^2 sin(phi - psi)) = 0,
  dot.double(psi) + omega^2 sin(psi) + l_1 / l_2 (dot.double(phi) cos(phi - psi) - dot(phi)^2 sin(phi - psi)) = 0
),
$ <mainFormula>

где $Omega^2 = g / l_1$, $omega^2 = g / l_2$, $M = m_1 + m_2$.

Эта система не решается аналитически, однако в предположении малых колебаний $phi, psi -> 0$ можно получить следующее:

$
cases(
  dot.double(phi) + Omega^2 phi + m_2 / M l_2 / l_1 dot.double(psi) = 0,
  dot.double(psi) + omega^2 psi + l_1 / l_2 dot.double(phi) = 0,
).
$

== Модель в реализации

Система @mainFormula не позволит найти точное решение задачи, однако можно получить выражения для $dot.double(phi)$ и $dot.double(psi)$, которые удобно использовать в коде модели. Формула, испоьзующаяся в проекте:

$
cases(
  dot.double(phi) = (-g (2 m_1 + m_2) sin(phi) - m_2 g sin(phi - 2 psi) - 2 sin(phi - psi) m_2 (dot(psi)^2 l_2 + dot(phi)^2 l_1 cos(phi - psi))) / (l_1 (2 m_1 + m_2 - m_2 cos(2 phi - 2 psi))),
  dot.double(psi) = ((2 sin(phi - psi) (dot(phi)^2 l_1 (m_1 + m_2) + g (m_1 + m_2) cos(phi) + dot(psi)^2 l_2 m_2 cos(phi - psi)))) / (l_1 (2 m_1 + m_2 - m_2 cos(2 phi - 2 psi)))
).
$


== Источники

- #link("https://ru.wikipedia.org/wiki/%D0%94%D0%B2%D0%BE%D0%B9%D0%BD%D0%BE%D0%B9_%D0%BC%D0%B0%D1%8F%D1%82%D0%BD%D0%B8%D0%BA")[Википедия. Двойной маятник].
- #link("https://www.youtube.com/watch?v=gxHnS3g0a98")[YouTube. TheorPhys Psu].
