package drawer

import (
	"image/color"

	. "double_pendulum/engineer"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

type Drawer struct {
	transl Point
}

func NewDrawer(translation Point) Drawer {
	return Drawer{
		transl: translation,
	}
}

func (dr *Drawer) DrawPoint(screen *ebiten.Image, center Point, radius float32, vecColor color.Color) {
	vector.DrawFilledCircle(
		screen,
		float32(center.X+dr.transl.X),
		float32(center.Y+dr.transl.Y),
		radius,
		vecColor,
		false,
	)
}

func (dr *Drawer) drawVec(screen *ebiten.Image, x0, y0 float64, x1 float64, y1 float64, vecColor color.Color) {
	vector.StrokeLine(
		screen,
		float32(x0+dr.transl.X),
		float32(y0+dr.transl.Y),
		float32(x1+dr.transl.X),
		float32(y1+dr.transl.Y),
		6,
		vecColor,
		false,
	)
}

func (dr *Drawer) DrawSegm(screen *ebiten.Image, p0 Point, p1 Point, vecColor color.Color) {
	dr.drawVec(screen, p0.X, p0.Y, p1.X, p1.Y, vecColor)
	dr.DrawPoint(screen, p0, 10, vecColor)
	dr.DrawPoint(screen, p1, 10, vecColor)
}

func (dr *Drawer) DrawPendulum(screen *ebiten.Image, pend *DoublePendulum, vecColor color.Color) {
	p1, p2 := pend.GetCoordinates()
	dr.DrawSegm(screen, Point{X: 0, Y: 0}, p1, vecColor)
	dr.DrawSegm(screen, p1, p2, vecColor)
}

func (dr *Drawer) DrawPendulumTail(screen *ebiten.Image, pend *DoublePendulum, vecColor color.Color) {
	_, p2 := pend.GetCoordinates()
	dr.DrawPoint(screen, p2, 10, vecColor)
}

func (dr *Drawer) DrawTrace(screen *ebiten.Image, trace *Trace, vecColor color.Color) {
	for _, point := range trace.Points {
		dr.DrawPoint(screen, point, 3, vecColor)
	}
}
