package drawer

import (
	"image/color"
)

var FromColor = color.RGBA{0, 191, 255, 255}
var ToColor = color.RGBA{100, 241, 255, 255}

func linearGradient(value float32, maxValue float32, fromColor color.Color,
	toColor color.Color) (uint8, uint8, uint8) {
	ratio := value / maxValue

	r1, g1, b1, _ := fromColor.RGBA()
	r2, g2, b2, _ := toColor.RGBA()
	r1, g1, b1 = r1%256, g1%256, b1%256
	r2, g2, b2 = r2%256, g2%256, b2%256

	r := float32(r1) + ratio*float32(r2-r1)
	g := float32(g1) + ratio*float32(g2-g1)
	b := float32(b1) + ratio*float32(b2-b1)

	return uint8(r), uint8(g), uint8(b)
}

func GetGradient(value uint32, maxValue uint32) color.Color {
	red, green, blue := linearGradient(float32(value), float32(maxValue), FromColor, ToColor)

	return color.RGBA{uint8(red), uint8(green), uint8(blue), 255}
}
