module double_pendulum

go 1.21.3

require (
	github.com/hajimehoshi/ebiten/v2 v2.6.3
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/ebitengine/purego v0.5.0 // indirect
	github.com/jezek/xgb v1.1.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	golang.org/x/exp/shiny v0.0.0-20230817173708-d852ddb80c63 // indirect
	golang.org/x/image v0.12.0 // indirect
	golang.org/x/mobile v0.0.0-20230922142353-e2f452493d57 // indirect
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
)
